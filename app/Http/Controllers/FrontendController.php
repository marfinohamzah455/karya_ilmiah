<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class FrontendController extends Controller
{
    public function index(){
        $categories = \App\Models\Category::with(["catalogs" => function($q){
            $q->where('status', '=', 1);
        }])->get();
        $recents = \App\Models\Catalog::with('catalog_metadata_value')->where('status','1')->orderBy('created_at', 'desc')->get();
        // with('catalogs')->get();
        // return response($recents);
        return view('frontEnd.index')->with(compact('categories'))->with(compact('recents'));
    }

    public function detailCategory($id){
        $category = \App\Models\Category::with(["catalogs" => function($q){
            $q->where('status', '=', 1);
        }])->where('id',$id)->first();
        return view('frontEnd.detail_category')->with(compact('category'));

    }

    public function detailCatalog($id){
        $catalog = \App\Models\Catalog::with('catalog_metadata_value')->where('status','1')->where('id',$id)->first();
        // return response($catalog);
        return view('frontEnd.detail_catalog')->with(compact('catalog'));
        
    }

    public function search(Request $request){
        $categories = \App\Models\Category::pluck('name','id');
        $categories->offsetSet(0,'ALL');
        $catalog = \App\Models\Catalog::with('catalog_metadata_value')->whereHas('catalog_metadata_value', function ($query) {
            $query->where('value','LIKE','%'.request('q').'%');
        })->when( $request->has('cat_id'),function($query)  use ($request) {
            $query->where('category_id', $request->input('cat_id'));
        })->where('status', '=', 1)->get();
   
        // return response($categories);
        return view('frontEnd.detail_search')->with(compact('catalog'))->with(compact('categories'));

    }

    
}
