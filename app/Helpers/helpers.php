<?php

function get_metadata_value($array,$key){
    foreach ($array as $row) {
        if($row['metadata_key']==$key) return $row['value'];
    }
}

function get_metadata_value_id($array,$key){
    foreach ($array as $row) {
        if($row['metadata_key']==$key) return $row['id'];
    }
}