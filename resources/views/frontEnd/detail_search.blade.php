@extends('frontEnd.layout')

@section('content')
<div class="widget">
    <div class="widget-header">
        <h3>Hasil pencarian "{{request('q')}}"</h3>
    </div>
    
    <div class="widget-content">
        <form action="{{route('search')}}" method="get" class="form form-inline">
                <div class="row">
                    <div class="col-md-5">
                       {{ Form::select('cat_id',$categories,request('cat_id') ? request('cat_id') :0,['class'=>'form-control']) }}
                    </div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="q" value="{{request('q')}}">
                            <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
        @foreach ($catalog as $key=>$item)
        <p>
            <strong>
                <a href="{{route('detail_catalog',$item->id)}}">
                {{ get_metadata_value($item['catalog_metadata_value'],'title') }}
                </a>
            </strong><br>
            <em>
                {{ get_metadata_value($item['catalog_metadata_value'],'author') }}
                {{ get_metadata_value($item['catalog_metadata_value'],'date') }}
            </em><br>
            <small>
                {{ Str::limit(get_metadata_value($item['catalog_metadata_value'],'abstrak'),200,'...') }}
            </small>
        </p>
        <hr>
        @endforeach
    </div>
</div>    
@endsection