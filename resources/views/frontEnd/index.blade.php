@extends('frontEnd.layout')

@section('content')
<div class="widget">
    <div class="widget-header">
        <h2>Repository STMA TRISAKTI</h2>
    </div>
    <div class="widget-content">
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perferendis quod maiores ipsum non excepturi voluptate incidunt. Asperiores nam quibusdam id natus aliquid sunt, corporis, perspiciatis quasi nobis accusantium obcaecati amet!</p>
    </div>
</div>
<hr>
<div class="widget">
    <div class="widget-header">
        <h3>Main Collection</h3>
    </div>
    <div class="widget-content">
        @foreach ($categories as $item)
           <p>
               <strong>
                    <a href="{{route('detail_category',$item->id)}}">
                        {{$item->name}} ({{$item->catalogs->count()}}) 
                    </a>
                </strong><br>
               <em>{{$item->description}}</em>
           </p>
        @endforeach
    </div>
</div>
<hr>
<div class="widget">
    <div class="widget-header">
        <h3>Recently Added</h3>
    </div>
    <div class="widget-content">
        @foreach ($recents as $key=>$item)
        <p>
            <strong>
                @foreach ($item['catalog_metadata_value'] as $row)
                    @if ($row['metadata_key']=='title')
                    <a href="{{route('detail_catalog',$item->id)}}">
                        {!! $row['value'] !!}     
                    </a>         
                    @endif
                @endforeach
            </strong><br>
            <em>
            @foreach ($item['catalog_metadata_value'] as $row)
                @if ($row['metadata_key']=='author')
                 {!! $row['value'] !!}              
                @endif
            @endforeach
            @foreach ($item['catalog_metadata_value'] as $row)
                @if ($row['metadata_key']=='date')
                ({!! $row['value'] !!})         
                @endif
            @endforeach
            </em><br>
            <small>
                @foreach ($item['catalog_metadata_value'] as $row)
                @if ($row['metadata_key']=='abstrak')
                {!! Str::limit($row['value'],100,'...') !!}         
                @endif
            @endforeach
            </small>
        </p>
        <hr>
        @endforeach
    </div>
</div>    
@endsection