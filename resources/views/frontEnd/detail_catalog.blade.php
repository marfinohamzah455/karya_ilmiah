@extends('frontEnd.layout')

@section('content')
<div class="widget">
    <div class="widget-header">
        <h3>
        {{ get_metadata_value($catalog['catalog_metadata_value'],'title') }}
        </h3>
        <span>
            {{ get_metadata_value($catalog['catalog_metadata_value'],'author') }},
            {{ get_metadata_value($catalog['catalog_metadata_value'],'date') }}
        </span>
        <div class="widget-content">
            {{ get_metadata_value($catalog['catalog_metadata_value'],'abstrak') }}
            <div class="col-md-12 pull-right" style="margin-top: 50px;border-top:1px solid #cecece;border-bottom:1px solid #cecece;">
                <a href="{{ asset('catalogs/cover'.$catalog['cover']) }}">
                    <i class="fa fa-download"></i> Download Cover
                </a>
                <span style="margin:0px 10px"></span>
                @auth
                <a href="{{ asset('catalogs/cover'.$catalog['full']) }}">
                    <i class="fa fa-download"></i> Download Full
                </a>
                @endauth  
                @auth
            <div style=";margin-top:20px">
                <h5>Full Item</h5>
                <table class="table">
                    @foreach ($catalog['catalog_metadata_value'] as $item)
                    @if ($item->metadata->katalog_metadata[0]->type == 1)
                    <tr>
                        <th class="col-md-2">{{$item->metadata->value}}</th>
                        <td class="col-md-4">{{$item->value}}</td>
                    </tr>                
                    @endif
                    @endforeach
                </table>
            </div>
            @endauth
            </div>
            
        </div>
    </div>
    
   
</div>    
@endsection