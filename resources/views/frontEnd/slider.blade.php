<section id="featured">
    <!-- start slider -->
    <!-- Slider -->
                                                    
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                                                                <li>
                        <img src="{{asset('frontEnd/img/slides/1.jpg')}}"
                             alt="Banner #1"/>
                        <div class="flex-caption">
                                                                <h3>Banner #1</h3>
                                                                                                <p>It is a long established fact that a reader will be distracted by the readable content of a page.</p>
                                                                                                <a href="#"
                                   class="btn btn-theme">More Details</a>
                                                        </div>
                    </li>
                                                                <li>
                        <img src="{{asset('frontEnd/img/slides/2.jpg')}}"
                             alt="Banner #2"/>
                        <div class="flex-caption">
                                                                <h3>Banner #2</h3>
                                                                                                <p>It is a long established fact that a reader will be distracted by the readable content of a page.</p>
                                                                                                <a href="#"
                                   class="btn btn-theme">More Details</a>
                                                        </div>
                    </li>
                                                                <li>
                        <img src="{{asset('frontEnd/img/slides/3.jpg')}}"
                             alt="Banner #3"/>
                        <div class="flex-caption">
                                                                <h3>Banner #3</h3>
                                                                                                <p>It is a long established fact that a reader will be distracted by the readable content of a page.</p>
                                                                                                <a href="#"
                                   class="btn btn-theme">More Details</a>
                                                        </div>
                    </li>
                                </ul>
        </div>
        <!-- end slider -->
</section>
<!-- end Home Slider -->