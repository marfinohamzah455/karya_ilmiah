@extends('frontEnd.layout')

@section('content')
<div class="widget">
    <div class="widget-header">
        <h3>{{$category->name}}</h3>
    </div>
    
    <div class="widget-content">
        @foreach ($category->catalogs as $key=>$item)
        <p>
            <strong>
                <a href="{{route('detail_catalog',$item->id)}}">
                {{ get_metadata_value($item['catalog_metadata_value'],'title') }}
                </a>
            </strong><br>
            <em>
                {{ get_metadata_value($item['catalog_metadata_value'],'author') }}
                {{ get_metadata_value($item['catalog_metadata_value'],'date') }}
            </em><br>
            <small>
                {{ Str::limit(get_metadata_value($item['catalog_metadata_value'],'abstrak'),100,'...') }}
            </small>
        </p>
        <hr>
        @endforeach
    </div>
</div>    
@endsection