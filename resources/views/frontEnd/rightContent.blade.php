<div class="widget">
    <div class="widget-header" style="margin-bottom: 10px">
        <form action="{{route('search')}}" method="get">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="q" value="{{request('q')}}">
            <div class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
            </div>
        </div>
    </form>

    </div>
    <div class="widget-content">
        <div class="list-group">
            <a href="#" class="list-group-item active">
                <i class="fa fa-table"></i> <span>Browse All Collections</span>
            </a>
            <a href="#" class="list-group-item"><span>Collections</span></a>
            <a href="#" class="list-group-item"><span>By Issued Date</span></a>
            <a href="#" class="list-group-item"><span>Authors</span></a>
            <a href="#" class="list-group-item"><span>Titles</span></a>
            <a href="#" class="list-group-item"><span>Subjects</span></a>
        
          </div>
          <div class="list-group">
            <a href="#" class="list-group-item active">
                <i class="fa fa-table"></i> <span>Browse All Collections</span>
            </a>
            <a href="#" class="list-group-item"><span>Collections</span></a>
            <a href="#" class="list-group-item"><span>By Issued Date</span></a>
            <a href="#" class="list-group-item"><span>Authors</span></a>
            <a href="#" class="list-group-item"><span>Titles</span></a>
            <a href="#" class="list-group-item"><span>Subjects</span></a>
        
          </div>
    </div>
</div>